# README #

### Program description: ###
The program compares the two input files, finds their differences, classifies the result by operations, and writes it to the output file. The program receives four parameters as input: the path to the first input file, the path to the second input file, the path to the output file and primary key. The output file format should be csv. First and second parameters must be set for the program to work.

### Examples of starting the program: ###
* python homework2.py -first_input_file='report_1.csv' -second_input_file='report_2.csv' -key=id -output_file='diff.csv'

### Reference: ###
* python homework2.py -h

### Output example ###
```
Operation	Date	                  id	     Cost	               Clicks	      note
added	    06-08-2020	              h0qcXGKj	 3.71335	           43	          Dean;45;12712
removed	    06-08-2020		                     3.71335	           43	          Dean;45;12712
changed		                          jd2NbVU7	 7.64346 => 7.64364		
changed		                          4_K2c7J1		                   41,0 => 41	
changed	    2020-30-11 => 30-11-2020  Ugth2DvF			
changed		                          CRBi0XuI	 нпуыа; => 7.87655		
changed		                          5PkqwwI5			                              Sophie;27:84363 => Sophie;27;84363
changed		                          mVypFsNq			                              Elеna;48;131042 => Elena;48;131042
changed		                          JU0DIV0I		                   null => 7	
changed	    R&8^ => 24-09-2020	      EX0fmA0x			
changed		                          AT1hwCWG			                              Hailey;31;373  
```