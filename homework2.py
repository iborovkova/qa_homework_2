import sys
import argparse
import csv
from enum import Enum
import subprocess

class DictionaryKeys(str, Enum):
    ADDED = 'added'
    REMOVED = 'removed'
    CHANGED = 'changed'
    CHANGES = 'changes'
    KEY = 'key'

if __name__ == '__main__':

    subprocess.run('pip install csv-diff')
    from  csv_diff import load_csv, compare
    
    try:
        # set console arguments
        args_parser = argparse.ArgumentParser(description = 'Compare and find the differences for two files')
        args_parser.add_argument('-first_input_file', type = str, default = '', help = 'Path first input file')
        args_parser.add_argument('-second_input_file', type = str, default = '', help = 'Path second input file')
        args_parser.add_argument('-output_file', type = str, default = 'diff.csv', help = 'Path output file')
        args_parser.add_argument('-key', type = str, default = 'id', help = 'Primary key')
        args = args_parser.parse_args()
    except:
        print('Arguments are not correct.')
        exit(0)

    if ((not args.first_input_file) or (not args.second_input_file)):
        print('Arguments not contain input file keys.')
        exit(0)
    
    try:
        with open(args.first_input_file, 'r') as f:
            reader = csv.reader(f, delimiter = '\t')
            headers_1 = next(reader)

        with open(args.second_input_file, 'r') as f:
            reader = csv.reader(f, delimiter = '\t')
            headers_2 = next(reader)
    except:
        print('First or second input file can be open.')
        exit(0)

    if headers_1 != headers_2:
        print('Headers not equals.')
        exit(0) 
    
    if  not args.key in headers_1:
        print('Incorrect key selected')
        exit(0)

    diff = compare(
        load_csv(open(args.first_input_file), key = args.key),
        load_csv(open(args.second_input_file), key = args.key)
    )
        
    try:
        with open(args.output_file, 'w') as differences:
            writer = csv.writer(differences, delimiter = '\t', dialect = 'excel', lineterminator = '\r')

            # Headers
            writer.writerow(['Operation'] + headers_1)

            # Added
            if DictionaryKeys.ADDED in diff:
                for parameters in diff[DictionaryKeys.ADDED]:
                    row = [DictionaryKeys.ADDED]
                    for header in headers_1:
                        row.append(parameters[header])
                    writer.writerow(row)

            # Removed
            if DictionaryKeys.REMOVED in diff:
                for parameters in diff[DictionaryKeys.REMOVED]:
                    row = [DictionaryKeys.REMOVED]
                    for header in headers_1:
                        row.append(parameters[header])
                    writer.writerow(row)

            # Changed
            if DictionaryKeys.CHANGED in diff:
                for parameters in diff[DictionaryKeys.CHANGED]:
                    row = [DictionaryKeys.CHANGED]
                    for header in headers_1:
                        if header == args.key:
                            row.append(parameters[DictionaryKeys.KEY])
                        elif header in parameters[DictionaryKeys.CHANGES]:
                            row.append(f'{parameters[DictionaryKeys.CHANGES][header][0]} => {parameters[DictionaryKeys.CHANGES][header][1]}')
                        else:
                            row.append('')
                    writer.writerow(row)

    except:
        print('Error writing to output file.')
